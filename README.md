# GYMSHARK Engineering Challenge

Android App that will allow customers to see the range of products on sale at **GYMSHARK**.

## Architecture Design

The Project follows a MVVM with Repository pattern architecture. This architecture was chosen for:

- Separation of Concerns that provides a way to testing the architecture components in isolation and allows for the View classes to be updated without modifying the ViewModel classes.
- Resilience to configuration changes allows the ViewModel classes to store UI data that would otherwise be lost on screen rotation or activity lifecycle changes.

## Project Technology

- Kotlin
- Flow (Coroutines)
- Jetpack Compose _+navigation-compose_
- Navigation Component
- Dagger Hilt

## Main Libraries Used

| Libraries                                                                               | _use_                                       |
|-----------------------------------------------------------------------------------------|---------------------------------------------|
| [Dagger Hilt](https://developer.android.com/training/dependency-injection/hilt-android) | Provide dependency injection                |
| [Retrofit](https://square.github.io/retrofit/)                                          | Provide access to the backend API endpoints |
| [Coil](https://coil-kt.github.io/coil/)                                                 | Displaying and managing images              |
| [Mockk](https://mockk.io/)                                                              | Create mock objects for testing purposes    |

___

## Data

API EndPoint

**Main List Feed** :
https://cdn.develop.gymshark.com/training/mock-product-responses/algolia-example-payload.json

___

### Assumptions

- Data is not to be stored locally on device.
- Fixed Portrait Compact layouts is sufficient.
- Dark mode or dynamic colour not required.
- Price is in Pounds(£).

### Further Improvements

- App layouts to be responsive and adaptive as to support as many screen sizes as possible.
- Refactor to cache data in Room DB.
- Dark theme support.
- Multi-screen support.