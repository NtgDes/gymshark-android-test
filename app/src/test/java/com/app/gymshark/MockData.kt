package com.app.gymshark

import android.util.Log
import com.app.gymshark.data.remote.Hit
import com.app.gymshark.data.remote.Media
import com.app.gymshark.data.remote.ProductListDTO
import com.app.gymshark.domain.model.Product
import io.mockk.coEvery
import io.mockk.mockkStatic

val mockProductList = listOf(
    Product(
        ID = 6732609257571,
        inStock = true,
        sizeInStock = listOf(
            "xs",
            "l",
            "xl",
            "xxl"
        ),
        title = "Speed Leggings",
        description = "<meta charset=\"utf-8\">\n<p data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">RUN WITH IT</strong></p>\n<p data-mce-fragment=\"1\"><br data-mce-fragment=\"1\">Your run requires enduring comfort and support, so step out and hit the road in Speed. Made with zero-distractions and lightweight, ventilating fabrics that move with you, you can trust in Speed no matter how far you go.</p>\n<p data-mce-fragment=\"1\"> <br data-mce-fragment=\"1\"></p>\n<p data-mce-fragment=\"1\"><br data-mce-fragment=\"1\">- Full length legging<br data-mce-fragment=\"1\">- High-waisted<br data-mce-fragment=\"1\">- Compressive fit<br data-mce-fragment=\"1\">- Internal adjustable elastic/drawcord at front waistband<br data-mce-fragment=\"1\">- Pocket to back of waistband<br data-mce-fragment=\"1\">- Reflective Gymshark sharkhead logo to ankle<br data-mce-fragment=\"1\">- Main: 88% Polyester 12% Elastane. Internal Mesh: 76% Nylon 24% Elastane<br data-mce-fragment=\"1\">- We've cut down our use of swing tags, so this product comes without one<br data-mce-fragment=\"1\">- Model is <meta charset=\"utf-8\"><span data-mce-fragment=\"1\" lang=\"EN-GB\" class=\"TextRun SCXP103297068 BCX0\" data-contrast=\"none\" data-usefontface=\"true\" xml:lang=\"EN-GB\"><span data-mce-fragment=\"1\" class=\"NormalTextRun SCXP103297068 BCX0\">5'3\" and wears a size M</span></span><br data-mce-fragment=\"1\">- SKU: B3A3E-UBCY</p>",
        type = "Womens Leggings",
        gender = listOf("f"),
        fit = null,
        labels = null,
        colour = "Navy",
        price = 1000,
        images = listOf(
            "https://cdn.shopify.com/s/files/1/1326/4923/products/SpeedLEGGINGNavy-B3A3E-UBCY.A-Edit_BK.jpg?v=1649254794",
            "https://cdn.shopify.com/s/files/1/1326/4923/products/SpeedLEGGINGNavy-B3A3E-UBCY.B-Edit_BK.jpg?v=1649254794",
            "https://cdn.shopify.com/s/files/1/1326/4923/products/SpeedLEGGINGNavy-B3A3E-UBCY.C-Edit_BK.jpg?v=1649254794",
        ),
    ),
    Product(
        ID = 6732607094883,
        inStock = true,
        sizeInStock = listOf(
            "xs",
            "s",
            "l",
            "xl",
            "xxl"
        ),
        title = "Speed Leggings",
        description = "<meta charset=\"utf-8\">\n<p data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">RUN WITH IT</strong></p>\n<p data-mce-fragment=\"1\"><br data-mce-fragment=\"1\">Your run requires enduring comfort and support, so step out and hit the road in Speed. Made with zero-distractions and lightweight, ventilating fabrics that move with you, you can trust in Speed no matter how far you go.</p>\n<p data-mce-fragment=\"1\"> <br data-mce-fragment=\"1\"></p>\n<p data-mce-fragment=\"1\"><br data-mce-fragment=\"1\">- Full length legging<br data-mce-fragment=\"1\">- High-waisted<br data-mce-fragment=\"1\">- Compressive fit<br data-mce-fragment=\"1\">- Internal adjustable elastic/drawcord at front waistband<br data-mce-fragment=\"1\">- Pocket to back of waistband<br data-mce-fragment=\"1\">- Reflective Gymshark sharkhead logo to ankle<br data-mce-fragment=\"1\">- Main: 88% Polyester 12% Elastane. Internal Mesh: 76% Nylon 24% Elastane<br data-mce-fragment=\"1\">- We've cut down our use of swing tags, so this product comes without one<br data-mce-fragment=\"1\">- Model is <meta charset=\"utf-8\"><span data-mce-fragment=\"1\"> </span><meta charset=\"utf-8\"><span lang=\"EN-GB\" class=\"TextRun SCXP30618988 BCX0\" data-contrast=\"none\" data-usefontface=\"true\" data-mce-fragment=\"1\" xml:lang=\"EN-GB\"><span class=\"NormalTextRun SCXP30618988 BCX0\" data-mce-fragment=\"1\">5'3\" and wears a size M</span></span><br>- Video model is <meta charset=\"utf-8\"><span data-usefontface=\"false\" data-contrast=\"none\" class=\"TextRun SCXP257442623 BCX0\" lang=\"EN-GB\" data-mce-fragment=\"1\" xml:lang=\"EN-GB\"><span class=\"NormalTextRun SCXP257442623 BCX0\" data-mce-fragment=\"1\">5’8” and wears a size XS</span></span><br data-mce-fragment=\"1\">- SKU: B3A3E-UBFC</p>",
        type = "Womens Leggings",
        gender = listOf("f"),
        fit = "mid-rise",
        labels = listOf("going-fast"),
        colour = "Moonstone Blue",
        price = 1000,
        images = listOf(
            "https://cdn.shopify.com/s/files/1/1326/4923/products/SpeedLeggingsMoonstoneBlue-B3A3E-UBFC.A_ZH_ZH.jpg?v=1649148949",
            "https://cdn.shopify.com/s/files/1/1326/4923/products/SpeedLeggingsMoonstoneBlue-B3A3E-UBFC.B_ZH_ZH.jpg?v=1649148949",
            "https://cdn.shopify.com/s/files/1/1326/4923/products/SpeedLeggingsMoonstoneBlue-B3A3E-UBFC.C_ZH_ZH.jpg?v=1649148949",
        ),
    ),
    Product(
        ID = 6732605882467,
        inStock = true,
        sizeInStock = listOf(
            "xs",
            "s",
            "l",
            "xl",
            "xxl"
        ),
        title = "Speed Leggings",
        description = "<p><strong>RUN WITH IT</strong></p>\n<p><br data-mce-fragment=\"1\">Your run requires enduring comfort and support, so step out and hit the road in Speed. Made with zero-distractions and lightweight, ventilating fabrics that move with you, you can trust in Speed no matter how far you go.</p>\n<p> </p>\n<p><br data-mce-fragment=\"1\">- Full length legging<br data-mce-fragment=\"1\">- High-waisted<br data-mce-fragment=\"1\">- Compressive fit<br data-mce-fragment=\"1\">- Internal adjustable elastic/drawcord at front waistband<br data-mce-fragment=\"1\">- Pocket to back of waistband<br data-mce-fragment=\"1\">- Reflective Gymshark sharkhead logo to ankle<br data-mce-fragment=\"1\">- Main: 88% Polyester 12% Elastane. Internal Mesh: 76% Nylon 24% Elastane<br data-mce-fragment=\"1\">- We've cut down our use of swing tags, so this product comes without one<br data-mce-fragment=\"1\">- Model is <meta charset=\"utf-8\"><span data-usefontface=\"true\" data-contrast=\"none\" class=\"TextRun SCXP103297068 BCX0\" lang=\"EN-GB\" data-mce-fragment=\"1\" xml:lang=\"EN-GB\"><span class=\"NormalTextRun SCXP103297068 BCX0\" data-mce-fragment=\"1\">5'3\" and wears a size M</span></span><br>- SKU: B3A3E-BBBB</p>",
        type = "Womens Leggings",
        gender = listOf("f"),
        fit = null,
        labels = null,
        colour = "Black",
        price = 1000,
        images = listOf(
            "https://cdn.shopify.com/s/files/1/1326/4923/products/SpeedLEGGINGBlack-B3A3E-BBBB.A-Edit_BK.jpg?v=1649254793"

        )
    )
)

val mockProductListDTO = ProductListDTO(
    listOf(
        Hit(
            id = 6732609257571,
            inStock = true,
            sizeInStock = listOf(
                "xs",
                "l",
                "xl",
                "xxl"
            ),
            title = "Speed Leggings",
            description = "<meta charset=\"utf-8\">\n<p data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">RUN WITH IT</strong></p>\n<p data-mce-fragment=\"1\"><br data-mce-fragment=\"1\">Your run requires enduring comfort and support, so step out and hit the road in Speed. Made with zero-distractions and lightweight, ventilating fabrics that move with you, you can trust in Speed no matter how far you go.</p>\n<p data-mce-fragment=\"1\"> <br data-mce-fragment=\"1\"></p>\n<p data-mce-fragment=\"1\"><br data-mce-fragment=\"1\">- Full length legging<br data-mce-fragment=\"1\">- High-waisted<br data-mce-fragment=\"1\">- Compressive fit<br data-mce-fragment=\"1\">- Internal adjustable elastic/drawcord at front waistband<br data-mce-fragment=\"1\">- Pocket to back of waistband<br data-mce-fragment=\"1\">- Reflective Gymshark sharkhead logo to ankle<br data-mce-fragment=\"1\">- Main: 88% Polyester 12% Elastane. Internal Mesh: 76% Nylon 24% Elastane<br data-mce-fragment=\"1\">- We've cut down our use of swing tags, so this product comes without one<br data-mce-fragment=\"1\">- Model is <meta charset=\"utf-8\"><span data-mce-fragment=\"1\" lang=\"EN-GB\" class=\"TextRun SCXP103297068 BCX0\" data-contrast=\"none\" data-usefontface=\"true\" xml:lang=\"EN-GB\"><span data-mce-fragment=\"1\" class=\"NormalTextRun SCXP103297068 BCX0\">5'3\" and wears a size M</span></span><br data-mce-fragment=\"1\">- SKU: B3A3E-UBCY</p>",
            type = "Womens Leggings",
            gender = listOf("f"),
            fit = null,
            labels = null,
            colour = "Navy",
            price = 1000,
            media = listOf(
                Media(src = "https://cdn.shopify.com/s/files/1/1326/4923/products/SpeedLEGGINGNavy-B3A3E-UBCY.A-Edit_BK.jpg?v=1649254794"),
                Media(src = "https://cdn.shopify.com/s/files/1/1326/4923/products/SpeedLEGGINGNavy-B3A3E-UBCY.B-Edit_BK.jpg?v=1649254794"),
                Media(src = "https://cdn.shopify.com/s/files/1/1326/4923/products/SpeedLEGGINGNavy-B3A3E-UBCY.C-Edit_BK.jpg?v=1649254794"),
            ),
        ),
        Hit(
            id = 6732607094883,
            inStock = true,
            sizeInStock = listOf(
                "xs",
                "s",
                "l",
                "xl",
                "xxl"
            ),
            title = "Speed Leggings",
            description = "<meta charset=\"utf-8\">\n<p data-mce-fragment=\"1\"><strong data-mce-fragment=\"1\">RUN WITH IT</strong></p>\n<p data-mce-fragment=\"1\"><br data-mce-fragment=\"1\">Your run requires enduring comfort and support, so step out and hit the road in Speed. Made with zero-distractions and lightweight, ventilating fabrics that move with you, you can trust in Speed no matter how far you go.</p>\n<p data-mce-fragment=\"1\"> <br data-mce-fragment=\"1\"></p>\n<p data-mce-fragment=\"1\"><br data-mce-fragment=\"1\">- Full length legging<br data-mce-fragment=\"1\">- High-waisted<br data-mce-fragment=\"1\">- Compressive fit<br data-mce-fragment=\"1\">- Internal adjustable elastic/drawcord at front waistband<br data-mce-fragment=\"1\">- Pocket to back of waistband<br data-mce-fragment=\"1\">- Reflective Gymshark sharkhead logo to ankle<br data-mce-fragment=\"1\">- Main: 88% Polyester 12% Elastane. Internal Mesh: 76% Nylon 24% Elastane<br data-mce-fragment=\"1\">- We've cut down our use of swing tags, so this product comes without one<br data-mce-fragment=\"1\">- Model is <meta charset=\"utf-8\"><span data-mce-fragment=\"1\"> </span><meta charset=\"utf-8\"><span lang=\"EN-GB\" class=\"TextRun SCXP30618988 BCX0\" data-contrast=\"none\" data-usefontface=\"true\" data-mce-fragment=\"1\" xml:lang=\"EN-GB\"><span class=\"NormalTextRun SCXP30618988 BCX0\" data-mce-fragment=\"1\">5'3\" and wears a size M</span></span><br>- Video model is <meta charset=\"utf-8\"><span data-usefontface=\"false\" data-contrast=\"none\" class=\"TextRun SCXP257442623 BCX0\" lang=\"EN-GB\" data-mce-fragment=\"1\" xml:lang=\"EN-GB\"><span class=\"NormalTextRun SCXP257442623 BCX0\" data-mce-fragment=\"1\">5’8” and wears a size XS</span></span><br data-mce-fragment=\"1\">- SKU: B3A3E-UBFC</p>",
            type = "Womens Leggings",
            gender = listOf("f"),
            fit = "mid-rise",
            labels = listOf("going-fast"),
            colour = "Moonstone Blue",
            price = 1000,
            media = listOf(
                Media("https://cdn.shopify.com/s/files/1/1326/4923/products/SpeedLeggingsMoonstoneBlue-B3A3E-UBFC.A_ZH_ZH.jpg?v=1649148949"),
                Media("https://cdn.shopify.com/s/files/1/1326/4923/products/SpeedLeggingsMoonstoneBlue-B3A3E-UBFC.B_ZH_ZH.jpg?v=1649148949"),
                Media("https://cdn.shopify.com/s/files/1/1326/4923/products/SpeedLeggingsMoonstoneBlue-B3A3E-UBFC.C_ZH_ZH.jpg?v=1649148949"),
            ),
        ),
        Hit(
            id = 6732605882467,
            inStock = true,
            sizeInStock = listOf(
                "xs",
                "s",
                "l",
                "xl",
                "xxl"
            ),
            title = "Speed Leggings",
            description = "<p><strong>RUN WITH IT</strong></p>\n<p><br data-mce-fragment=\"1\">Your run requires enduring comfort and support, so step out and hit the road in Speed. Made with zero-distractions and lightweight, ventilating fabrics that move with you, you can trust in Speed no matter how far you go.</p>\n<p> </p>\n<p><br data-mce-fragment=\"1\">- Full length legging<br data-mce-fragment=\"1\">- High-waisted<br data-mce-fragment=\"1\">- Compressive fit<br data-mce-fragment=\"1\">- Internal adjustable elastic/drawcord at front waistband<br data-mce-fragment=\"1\">- Pocket to back of waistband<br data-mce-fragment=\"1\">- Reflective Gymshark sharkhead logo to ankle<br data-mce-fragment=\"1\">- Main: 88% Polyester 12% Elastane. Internal Mesh: 76% Nylon 24% Elastane<br data-mce-fragment=\"1\">- We've cut down our use of swing tags, so this product comes without one<br data-mce-fragment=\"1\">- Model is <meta charset=\"utf-8\"><span data-usefontface=\"true\" data-contrast=\"none\" class=\"TextRun SCXP103297068 BCX0\" lang=\"EN-GB\" data-mce-fragment=\"1\" xml:lang=\"EN-GB\"><span class=\"NormalTextRun SCXP103297068 BCX0\" data-mce-fragment=\"1\">5'3\" and wears a size M</span></span><br>- SKU: B3A3E-BBBB</p>",
            type = "Womens Leggings",
            gender = listOf("f"),
            fit = null,
            labels = null,
            colour = "Black",
            price = 1000,
            media = listOf(
                Media("https://cdn.shopify.com/s/files/1/1326/4923/products/SpeedLEGGINGBlack-B3A3E-BBBB.A-Edit_BK.jpg?v=1649254793")

            ),
        )
    )
)

fun disableLogger() {
    mockkStatic(Log::class)
    coEvery { Log.e(any(), any()) } returns 0
}