package com.app.gymshark.data.remote

import com.app.gymshark.mockProductListDTO
import org.junit.Assert.assertEquals
import org.junit.Test

class ProductListDTOKtTest {
    @Test
    fun `Verify that ProductListDTO is correctly converted ProductList`() {
        val result = mockProductListDTO.toProductList()

        assertEquals(result.size, mockProductListDTO.hits.size)
        assertEquals(result[0].ID, mockProductListDTO.hits[0].id)
    }
}