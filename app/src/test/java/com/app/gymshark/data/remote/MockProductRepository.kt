package com.app.gymshark.data.remote

import com.app.gymshark.domain.model.Product
import com.app.gymshark.domain.repository.ProductRepository
import com.app.gymshark.utility.Resource
import kotlinx.coroutines.flow.flow

class MockProductRepository(
    private val emits: Emits = Emits.Retrieving,
    private val productList: List<Product>? = null,
    private val errorMessage: String = "Error"
) : ProductRepository {
    override suspend fun getProductList() = flow {
        emit(Resource.Retrieving())

        when (emits) {
            Emits.Success -> emit(Resource.Success(productList))
            Emits.Error -> emit(Resource.Error(message = errorMessage))
            else -> {}
        }
    }

    enum class Emits { Retrieving, Success, Error }
}