package com.app.gymshark.data

import com.app.gymshark.data.remote.Api
import com.app.gymshark.disableLogger
import com.app.gymshark.mockProductListDTO
import com.app.gymshark.utility.Resource
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockkClass
import kotlinx.coroutines.flow.last
import kotlinx.coroutines.test.runTest
import okhttp3.ResponseBody.Companion.toResponseBody
import org.junit.Assert
import org.junit.Test
import retrofit2.HttpException
import retrofit2.Response
import java.io.IOException

class ProductRepositoryImplTest {
    private val api = mockkClass(Api::class, relaxed = true)

    val logger = disableLogger()

    private val ProductRepositoryImpl = ProductRepositoryImpl(api = api)

    @Test
    fun `Verify that Product List  Api call was made`() = runTest {
        coEvery { api.productList() } returns mockProductListDTO

        ProductRepositoryImpl.getProductList().last()

        coVerify {
            api.productList()
        }
    }


    @Test
    fun `Product Api Error, returns error response`() = runTest {
        coEvery { api.productList() } throws IOException()

        val results = ProductRepositoryImpl.getProductList().last()

        Assert.assertTrue(results is Resource.Error)
    }

    @Test
    fun `Product Api Error, returns error message`() = runTest {
        val response: Response<Unit> = Response.error(404, "".toResponseBody(null))

        coEvery { api.productList() } throws HttpException(response)

        val results = ProductRepositoryImpl.getProductList().last()

        if (results is Resource.Error) Assert.assertTrue(results.message.isNotEmpty())
        else Assert.assertTrue(false)
    }
}