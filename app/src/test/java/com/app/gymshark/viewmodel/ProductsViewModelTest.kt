package com.app.gymshark.viewmodel

import com.app.gymshark.data.remote.MockProductRepository
import com.app.gymshark.disableLogger
import com.app.gymshark.domain.use_case.GetProductListUseCase
import com.app.gymshark.mockProductList
import com.app.gymshark.utility.Resource
import kotlinx.coroutines.test.runTest
import org.junit.Assert
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test

class ProductsViewModelTest {
    @get:Rule
    val mainDispatcherRule = DispatcherRule()

    val logger = disableLogger()

    private lateinit var viewModel: ProductsViewModel

    @Test
    fun `Start of Data-call has started`() = runTest {
        viewModel = ProductsViewModel(GetProductListUseCase(MockProductRepository()))

        val result = viewModel.productList.value
        Assert.assertTrue(result is Resource.Retrieving)
    }

    @Test
    fun `That Data-call has been retrieved correctly`() = runTest {
        viewModel = ProductsViewModel(
            GetProductListUseCase(
                MockProductRepository(
                    emits = MockProductRepository.Emits.Success,
                    productList = mockProductList
                )
            )
        )

        val result = viewModel.productList.value

        if (result is Resource.Success) assertEquals(result.data, mockProductList)
        else Assert.assertTrue(false)
    }

    @Test
    fun `That Product is selected retrieved correctly`() = runTest {
        viewModel = ProductsViewModel(
            GetProductListUseCase(
                MockProductRepository(
                    emits = MockProductRepository.Emits.Success,
                    productList = mockProductList
                )
            )
        )

        viewModel.setSelectedProduct(mockProductList[1].ID)

        val result = viewModel.selectedProduct

        assertEquals(result.value, mockProductList[1])

        viewModel.setSelectedProduct()

        assertEquals(result.value, null)
    }

    @Test
    fun `That data-call has failed`() = runTest {
        viewModel = ProductsViewModel(
            GetProductListUseCase(
                MockProductRepository(
                    emits = MockProductRepository.Emits.Error,
                    errorMessage = "IO"
                )
            )
        )

        val result = viewModel.productList.value

        if (result is Resource.Error) assertEquals(result.message, "IO")
        else Assert.assertTrue(false)
    }
}