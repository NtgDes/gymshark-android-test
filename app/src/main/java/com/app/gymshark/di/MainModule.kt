package com.app.gymshark.di

import com.app.gymshark.data.ProductRepositoryImpl
import com.app.gymshark.data.remote.Api
import com.app.gymshark.data.remote.baseUrl
import com.app.gymshark.domain.repository.ProductRepository
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object MainModule {

    @Provides
    @Singleton
    fun provideAPI(): Api = Retrofit.Builder()
        .baseUrl(baseUrl)
        .addConverterFactory(GsonConverterFactory.create(GsonBuilder().setLenient().create()))
        .build()
        .create(Api::class.java)

    @Provides
    @Singleton
    fun provideProductRepositoryImpl(api: Api): ProductRepository = ProductRepositoryImpl(api)
}