package com.app.gymshark.ui

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.app.gymshark.R
import com.app.gymshark.domain.model.Product
import com.app.gymshark.utility.stripHtml

@Composable
fun ProductDetail(product: Product) = Column(
    Modifier
        .verticalScroll(rememberScrollState())
        .fillMaxWidth()
        .padding(5.dp),
    horizontalAlignment = Alignment.CenterHorizontally,
    verticalArrangement = Arrangement.spacedBy(5.dp)
) {
    ImagePager(imageList = product.images)

    Text(
        text = product.title,
        style = MaterialTheme.typography.headlineSmall,
        textAlign = TextAlign.Center
    )

    Text(
        text = stripHtml(product.description),
        modifier = Modifier.padding(vertical = 5.dp)
    )

    if (!product.labels.isNullOrEmpty()) ItemGroup {

        Text(
            text = product.labels.joinToString(),
            textAlign = TextAlign.Center,
            modifier = Modifier.fillMaxWidth()
        )
    }

    ItemGroup {
        @Composable
        fun specificationItem(label: String, text: String) {
            HorizontalDivider()
            ItemDetails(label = label, text = text)
        }

        Text(
            text = stringResource(R.string.Details),
            style = MaterialTheme.typography.headlineSmall,
            modifier = Modifier
                .padding(bottom = 10.dp)
        )

        specificationItem(label = stringResource(R.string.Type), text = product.type)

        product.fit?.let { specificationItem(label = stringResource(R.string.Fit), text = it) }

        specificationItem(label = stringResource(R.string.Colour), text = product.colour)

        specificationItem(label = stringResource(R.string.In_stock), text = if (product.inStock) stringResource(R.string.yes) else stringResource(R.string.no))

        if (!product.sizeInStock.isNullOrEmpty()) specificationItem(label = stringResource(R.string.sizes), text = product.sizeInStock.joinToString())

        specificationItem(label = stringResource(R.string.Gender), text = product.gender.joinToString())
    }

    ItemGroup {
        ItemDetails(label = stringResource(R.string.price), text = "£ ${product.price}")
    }
}