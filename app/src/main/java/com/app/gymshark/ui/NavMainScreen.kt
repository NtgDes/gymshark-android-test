package com.app.gymshark.ui

sealed class NavMainScreen(val route: String) {
    data object ProductList : NavMainScreen("ProductList")
    data object Product : NavMainScreen("ProductDetail")
}