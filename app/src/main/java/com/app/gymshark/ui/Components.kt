package com.app.gymshark.ui

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.pager.HorizontalPager
import androidx.compose.foundation.pager.rememberPagerState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.KeyboardArrowLeft
import androidx.compose.material3.Card
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import coil.compose.AsyncImage
import com.app.gymshark.R
import com.app.gymshark.ui.theme.CustomTextStyle

@Composable
fun Title(
    title: String,
    navBack: (() -> Unit)? = null
) = Surface(
    modifier = Modifier.height(64.dp),
    shadowElevation = 5.dp
) {
    Row(
        modifier = Modifier.padding(end = 5.dp),
        verticalAlignment = Alignment.CenterVertically
    ) {
        navBack?.let {
            IconButton(onClick = { navBack() }) {
                Icon(
                    Icons.AutoMirrored.Default.KeyboardArrowLeft,
                    contentDescription = null,
                )
            }
        }

        Text(
            text = title,
            textAlign = TextAlign.Center,
            maxLines = 1,
            style = MaterialTheme.typography.headlineMedium,
            overflow = TextOverflow.Ellipsis,
            modifier = Modifier
                .padding(horizontal = 20.dp)
                .weight(1f)
                .fillMaxWidth()
        )

    }
}

@Composable
fun ItemGroup(
    heading: String? = null,
    modifier: Modifier = Modifier,
    content: @Composable ColumnScope.() -> Unit
) = Card {
    Column(
        modifier = modifier.padding(5.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        heading?.let {
            Text(
                text = heading,
                fontWeight = FontWeight.Black

            )
        }

        content()
    }
}

@Composable
fun ItemDetails(
    label: String,
    text: String,
    modifier: Modifier = Modifier
) = Row(
    modifier = modifier.fillMaxWidth(),
    horizontalArrangement = Arrangement.SpaceBetween
) {
    Text(
        text = label,
        fontWeight = FontWeight.Bold,
        modifier = Modifier.padding(end = 10.dp)

    )

    Text(text = text)
}

@Composable
fun StateView(state: String) {
    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        Text(
            text = state,
            style = CustomTextStyle.statText
        )
    }
}

@Composable
fun ImageModelView(imageURL: String, onDismissRequest: () -> Unit) = Dialog(onDismissRequest = { onDismissRequest() }) {
    AsyncImage(
        model = imageURL,
        placeholder = painterResource(id = R.drawable.file_download_black_24dp),
        error = painterResource(id = R.drawable.file_download_off_black_24dp),
        contentDescription = null,
        modifier = Modifier.fillMaxWidth()
    )
}


@OptIn(ExperimentalFoundationApi::class)
@Composable
fun ImagePager(
    imageList: List<String>,
    modifier: Modifier = Modifier,
) = Column(modifier = modifier) {
    val pagerState = rememberPagerState { imageList.count() }
    HorizontalPager(
        state = pagerState,
        modifier = Modifier.height(300.dp)
    ) { i ->
        var showImageModelView by remember { mutableStateOf(false) }

        AsyncImage(
            model = imageList[i],
            placeholder = painterResource(id = R.drawable.file_download_black_24dp),
            error = painterResource(id = R.drawable.file_download_off_black_24dp),
            contentDescription = null,
            modifier = Modifier
                .fillMaxWidth()
                .clickable {
                    showImageModelView = true
                }
        )

        if (showImageModelView) ImageModelView(imageURL = imageList[i]) {
            showImageModelView = false
        }
    }

    Spacer(modifier = Modifier.height(5.dp))

    PagerIndicator(
        count = imageList.size,
        current = pagerState.currentPage,
        modifier = Modifier.fillMaxWidth()
    )
}

@Composable
fun PagerIndicator(
    count: Int,
    current: Int,
    modifier: Modifier = Modifier,
    size: Dp = 10.dp,
    spacing: Dp = 5.dp,
    colorOn: Color = Color.Black,
    colorOff: Color = Color.Gray,
    indicatorShape: Shape = CircleShape
) = Row(
    modifier = modifier,
    horizontalArrangement = Arrangement.Center,
    verticalAlignment = Alignment.CenterVertically
) {
    for (i in 0 until count)
        Box(
            modifier = Modifier
                .padding(horizontal = spacing)
                .size(size)
                .background(color = if (i == current) colorOn else colorOff, shape = indicatorShape)
        )
}