package com.app.gymshark.ui

import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import com.app.gymshark.R
import com.app.gymshark.domain.model.Product
import com.app.gymshark.utility.Resource
import com.app.gymshark.viewmodel.ProductsViewModel

@Composable
fun ProductList(viewModel: ProductsViewModel, navToProduct: (productID: Long) -> Unit) {
    var title by remember { mutableStateOf("Product List") }

    Column {
        Title(title = title)

        when (val screenState = viewModel.productList.value) {
            is Resource.Success -> {
                val productList = screenState.data

                if (productList != null) {
                    title = "Product List (${productList.count()})"
                    HorizontalDivider()

                    LazyVerticalGrid(
                        columns = GridCells.Adaptive(minSize = 200.dp)
                    ) {
                        items(productList.size) { i ->
                            ProductListItem(productList[i]) { productID ->
                                navToProduct(productID)
                            }
                        }
                    }
                } else StateView(state = stringResource(R.string.not_found))

            }

            is Resource.Retrieving -> StateView(state = stringResource(R.string.loading))
            is Resource.Error -> StateView(state = stringResource(R.string.error, screenState.message))
        }
    }
}

@Composable
fun ProductListItem(
    product: Product,
    onClick: (productID: Long) -> Unit
) {
    Column(
        modifier = Modifier
            .clickable { onClick(product.ID) }
            .fillMaxWidth()
            .border(width = .1.dp, color = Color.Gray)
            .padding(20.dp)
            .aspectRatio(.6f)
            .semantics(mergeDescendants = true) { },
        horizontalAlignment = Alignment.CenterHorizontally
    ) {

        AsyncImage(
            model = product.images[0],
            placeholder = painterResource(id = R.drawable.file_download_black_24dp),
            error = painterResource(id = R.drawable.file_download_off_black_24dp),
            contentDescription = null,
            modifier = Modifier
                .fillMaxHeight(.1f)
                .weight(1f)
        )

        Column {
            Text(
                text = product.title
            )

            Text(
                text = "£${product.price}",
                fontWeight = FontWeight.ExtraBold
            )
        }
    }
}