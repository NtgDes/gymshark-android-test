package com.app.gymshark.viewmodel

import android.util.Log
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.app.gymshark.domain.model.Product
import com.app.gymshark.domain.use_case.GetProductListUseCase
import com.app.gymshark.utility.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ProductsViewModel @Inject constructor(
    private val getProductListUseCase: GetProductListUseCase
) : ViewModel() {

    private val _productList = mutableStateOf<Resource<List<Product>>>(Resource.Retrieving())
    val productList = _productList

    private val _selectedProduct = mutableStateOf<Product?>(null)
    val selectedProduct = _selectedProduct

    init {
        getProductList()
    }

    private fun getProductList() {
        viewModelScope.launch {
            getProductListUseCase().onEach { state ->
                _productList.value = state

                if (state is Resource.Error)
                    Log.e("ProductListViewModel", "getProductList() : " + state.message)
            }.launchIn(viewModelScope)
        }
    }

    fun setSelectedProduct(ID: Long? = null) {
        if (_productList.value is Resource.Success)
            _selectedProduct.value = _productList.value.data?.find { it.ID == ID }
    }
}