package com.app.gymshark

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.BackHandler
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.SideEffect
import androidx.compose.ui.Modifier
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.app.gymshark.ui.NavMainScreen
import com.app.gymshark.ui.ProductDetail
import com.app.gymshark.ui.ProductList
import com.app.gymshark.ui.Title
import com.app.gymshark.ui.theme.GYMSHARKTheme
import com.app.gymshark.viewmodel.ProductsViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val viewModel = hiltViewModel<ProductsViewModel>()

            GYMSHARKTheme {
                // A surface container using the 'background' color from the theme
                Surface(modifier = Modifier.fillMaxSize(), color = MaterialTheme.colorScheme.background) {

                    val navController = rememberNavController()

                    NavHost(navController = navController, startDestination = NavMainScreen.ProductList.route) {
                        composable(route = NavMainScreen.ProductList.route) {
                            ProductList(viewModel = viewModel) { productID ->
                                viewModel.setSelectedProduct(productID)
                                navController.navigate(NavMainScreen.Product.route)
                            }

                            SideEffect {
                                if (viewModel.selectedProduct.value != null && navController.currentDestination?.route == NavMainScreen.ProductList.route)
                                    navController.navigate(NavMainScreen.Product.route)
                            }
                        }

                        composable(route = NavMainScreen.Product.route) {
                            val product = viewModel.selectedProduct.value

                            fun navMasterList() {
                                viewModel.setSelectedProduct(null)
                                navController.popBackStack()
                            }

                            product?.run {
                                Column {
                                    Title(title = title) { navMasterList() }

                                    ProductDetail(product = product)
                                }
                            }

                            BackHandler {
                                navMasterList()
                            }
                        }
                    }
                }
            }
        }
    }
}