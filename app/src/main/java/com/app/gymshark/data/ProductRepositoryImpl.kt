package com.app.gymshark.data

import android.util.Log
import com.app.gymshark.data.remote.Api
import com.app.gymshark.data.remote.toProductList
import com.app.gymshark.domain.model.Product
import com.app.gymshark.domain.repository.ProductRepository
import com.app.gymshark.utility.Resource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException

class ProductRepositoryImpl(
    private val api: Api
) : ProductRepository {
    override suspend fun getProductList(): Flow<Resource<List<Product>>> = flow {
        emit(Resource.Retrieving())

        try {
            val productList = api.productList()

            emit(Resource.Success(productList.toProductList()))
        } catch (e: HttpException) {
            Log.e("ProductRepositoryImpl", "getProductList() : HttpException : " + e.localizedMessage)

            emit(Resource.Error(message = e.localizedMessage ?: "Network error"))
        } catch (e: IOException) {
            Log.e("ProductRepositoryImpl", "getProductList() : IOException : " + e.localizedMessage)

            emit(Resource.Error(message = e.localizedMessage ?: "Network error"))
        }
    }
}