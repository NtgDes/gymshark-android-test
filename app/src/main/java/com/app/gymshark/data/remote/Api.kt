package com.app.gymshark.data.remote

import retrofit2.http.GET

const val baseUrl = "https://cdn.develop.gymshark.com/training/mock-product-responses/"

interface Api {
    @GET("algolia-example-payload.json")
    suspend fun productList(): ProductListDTO
}