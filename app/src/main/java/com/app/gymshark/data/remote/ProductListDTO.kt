package com.app.gymshark.data.remote

import com.app.gymshark.domain.model.Product

class ProductListDTO(
    val hits: List<Hit>
)

data class Hit(
    val id: Long,
    val inStock: Boolean,
    val sizeInStock: List<String>?,
    val title: String,
    val description: String,
    val type: String,
    val gender: List<String>,
    val fit: String?,
    val labels: List<String>?,
    val colour: String,
    val price: Long,
    val media: List<Media>,
)

data class Media(
    val src: String,
)

fun ProductListDTO.toProductList() = hits.map {
    Product(
        ID = it.id,
        inStock = it.inStock,
        sizeInStock = it.sizeInStock,
        title = it.title,
        description = it.description,
        type = it.type,
        gender = it.gender,
        fit = it.fit,
        labels = it.labels,
        colour = it.colour,
        price = it.price,
        images = it.media.map { url -> url.src }
    )
}