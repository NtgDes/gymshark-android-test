package com.app.gymshark.domain.model

data class Product(
    val ID: Long,
    val inStock: Boolean,
    val sizeInStock: List<String>?,
    val title: String,
    val description: String,
    val type: String,
    val gender: List<String>,
    val fit: String?,
    val labels: List<String>?,
    val colour: String,
    val price: Long,
    val images: List<String>,
)