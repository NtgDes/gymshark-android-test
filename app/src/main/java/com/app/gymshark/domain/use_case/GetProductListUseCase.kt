package com.app.gymshark.domain.use_case

import com.app.gymshark.domain.repository.ProductRepository
import javax.inject.Inject

class GetProductListUseCase @Inject constructor(private val repository: ProductRepository) {
    suspend operator fun invoke() = repository.getProductList()
}