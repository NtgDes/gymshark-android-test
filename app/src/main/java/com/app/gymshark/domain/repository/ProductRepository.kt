package com.app.gymshark.domain.repository

import com.app.gymshark.domain.model.Product
import com.app.gymshark.utility.Resource
import kotlinx.coroutines.flow.Flow

interface ProductRepository {
    suspend fun getProductList(): Flow<Resource<List<Product>>>
}