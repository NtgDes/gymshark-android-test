package com.app.gymshark.utility

import android.text.Html

fun stripHtml(html: String) = Html.fromHtml(html, Html.FROM_HTML_MODE_COMPACT).toString()