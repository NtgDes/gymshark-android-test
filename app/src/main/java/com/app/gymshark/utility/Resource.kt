package com.app.gymshark.utility

sealed class Resource<T>(val data: T? = null) {
    class Retrieving<T> : Resource<T>()
    class Success<T>(data: T?) : Resource<T>(data)
    class Error<T>(val message: String) : Resource<T>()
}