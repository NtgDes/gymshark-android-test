package com.app.gymshark.ui

import androidx.compose.ui.test.hasClickAction
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithText
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ComponentsKtTest {
    @get:Rule
    val composable = createComposeRule()

    private val titleText = "Title"

    @Test
    fun title_navigate_back_visible() {
        composable.setContent {
            Title(titleText) {}
        }

        composable.onNodeWithText(titleText)
        composable.onNode(hasClickAction())
    }

    @Test
    fun title_navigate_back_not_visible() {
        composable.setContent {
            Title(titleText)
        }

        composable.onNodeWithText(titleText)
        composable.onNode(hasClickAction()).assertDoesNotExist()
    }
}