package com.app.gymshark.utility

import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class UtilityKtTest {
    @Test
    fun remove_HTML_Tags() {
        val result = stripHtml("<span><b>bold</b></span>")
        assertEquals(result, "bold")
    }
}